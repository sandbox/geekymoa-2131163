<?php

/**
 * @file
 * Theme setting callbacks for the Admin Suit theme.
 */

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * @param $form
 *   The form.
 * @param $form_state
 *   The form state.
 */
function admin_suit_form_system_theme_settings_alter(&$form, &$form_state) {

  if (module_exists('ds_forms')) {
    $form['vertical_tabs_in_layout'] = array(
      '#type' => 'fieldset',
      '#title' => t('Vertical tabs location settings'),
      '#description' => t('Select region to place vertical tabs in.'),
    );

    $ds_layouts = ds_get_layout_info();

    foreach ($ds_layouts as $machine_name => $layout) {

      array_unshift($layout['regions'], 'None');

      $form['vertical_tabs_in_layout']['vtil_' . $machine_name] = array(
        '#type' => 'select',
        '#title' => $layout['label'] . ': ',
        '#options' => $layout['regions'],
        '#default_value' => theme_get_setting('vtil_' . $machine_name),
      );
    }
  }

}


