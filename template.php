<?php

//function admin_suit_init() {
//  drupal_add_js(drupal_get_path('theme', 'admin_suit') . '/libraries/select2/select2.css');
//  drupal_add_css(drupal_get_path('theme', 'admin_suit') . '/libraries/select2/select2.min.js');
//}



function admin_suit_form_node_form_alter(&$form, &$form_state, $form_id) {

  if (module_exists('ds_forms')) {

    // Checks if there is a ds layout set
    $current_layout = ds_get_layout('node', $form['#node']->type, 'form', FALSE);

    if ($current_layout) {
      foreach($form as $key => $form_item) {

        // remove all items from group additional_settings
        if (is_array($form_item) && isset($form_item['#group']) && $form_item['#group'] == "additional_settings") {
          unset($form_item['#group']);
        }
      }

      unset($form['additional_settings']);
    }
  }
}


function admin_suit_ds_layout_region_settings_alter(&$layout) {

  $move_vertical_tabs = theme_get_setting('vtil_' . $layout['layout']);

  if ($move_vertical_tabs && $layout['view_mode'] == "form") {

    $layout['settings']['regions'][$move_vertical_tabs][] = 'menu';
    $layout['settings']['regions'][$move_vertical_tabs][] = 'author';
    $layout['settings']['regions'][$move_vertical_tabs][] = 'revision_information';
    $layout['settings']['regions'][$move_vertical_tabs][] = 'options';
    $layout['settings']['regions'][$move_vertical_tabs][] = 'ds_extras';

  }

}

function admin_suit_preprocess_html(&$variables) {

  if (isset($variables['page']['content']['system_main']['#node_edit_form']) && $variables['page']['content']['system_main']['#node_edit_form']) {
    $node = $variables['page']['content']['system_main']['#node'];
    $current_layout = ds_get_layout('node', $node->type, 'form', FALSE);
    $layout_class = 'layout-' . drupal_clean_css_identifier($current_layout['layout']);
    $variables['classes_array'][] = $layout_class;
  }

  $hi = 0;
}