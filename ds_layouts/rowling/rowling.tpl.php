<?php

/**
 * @file
 * Display Suite 2 column template.
 */
?>
<<?php print $layout_wrapper; print $layout_attributes; ?> class="admin-unit-rowling <?php print $classes;?> clearfix">

  <?php if (isset($title_suffix['contextual_links'])): ?>
  <?php print render($title_suffix['contextual_links']); ?>
  <?php endif; ?>

  <<?php print $main_wrapper ?> class="group-main<?php print $main_classes; ?>">
    <?php print $main; ?>
  </<?php print $main_wrapper ?>>

  <<?php print $metadata_wrapper ?> class="group-metadata<?php print $metadata_classes; ?>">
    <?php print $metadata; ?>
  </<?php print $metadata_wrapper ?>>

  <<?php print $publishing_wrapper ?> class="group-publishing<?php print $publishing_classes; ?>">
    <?php print $publishing; ?>
  </<?php print $publishing_wrapper ?>>

</<?php print $layout_wrapper ?>>

<?php if (!empty($drupal_render_children)): ?>
  <?php print $drupal_render_children ?>
<?php endif; ?>
