<?php

/**
 * @file
 * Display Suite austen configuration.
 */

function ds_austen() {

  return array(
    'label' => t('Austen (2 col 70/30)'),
    'regions' => array(
      'main' => t('Main'),
      'metadata' => t('Metadata'),
    ),
    // Uncomment if you want to include a CSS file for this layout (austen.css)
     'css' => TRUE,
    // Uncomment if you want to include a preview for this layout (austen.png)
    // 'image' => TRUE,
  );
}
